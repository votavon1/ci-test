# GitLab CI sample project

GitLab CI is a new and great feature of GitLab. This project is created to show how to
use it in the FEL GitLab instance.

## Useful links

* [CI documentation of GitLab](https://gitlab.fel.cvut.cz/help/ci/README.md)
* [CI with PHP and Docker based Runner example](https://gitlab.fel.cvut.cz/help/ci/examples/php.md)
* [CI and Docker based Runner in general](https://gitlab.fel.cvut.cz/help/ci/docker/using_docker_images.md)
* [CI lint - syntax checker of .gitlab-ci.yml](https://gitlab.fel.cvut.cz/ci/lint)

## Shared runners at gitlab.fel.cvut.cz

The shared runners are based on Docker. Please be sure to read the 
documentation above to use the correct image and services.

There is only one shared runner available at the moment. The capacity of the 
runner is 4 CPUs and 8 GB RAM. This should be enough for most of the workloads
the students create. 

In case you need a dedicated runner, don't hesitate to ask us. Use [HelpDesk FEL](https://helpdesk.fel.cvut.cz/otrs/customer.pl?Action=CustomerTicketMessage;Dest=67%7C%7CSVTI::Webov%C3%A9%20aplikace::Gitlab+repozit%C3%A1%C5%99;xDest=Gitlab+repozit%C3%A1%C5%99)
to ask.
