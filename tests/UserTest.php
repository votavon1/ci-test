<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {

  public function testDisplayName(){
    $user = new User('testeter', 1001, 'Tereza Tester');
    $this->assertEquals('Tereza Tester', $user->get_display_name());
  }

}
