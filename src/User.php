<?php

class User {
  private $username;
  private $uid;
  private $display_name;


  public function __construct($uname, $uid, $dname){
    $this->username = $uname;
    $this->uid = $uid;
    $this->display_name = $dname;
  }

  public function get_username(){
    return $this->username;
  }

  public function get_uid(){
    return $this->uid;
  }

  public function get_display_name(){
    return $this->display_name;
  }


}
